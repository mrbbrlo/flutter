import 'package:flutter/material.dart';

import 'colors.dart';

class TextStyles {
  static const header = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 24,
    color: AppColors.white,
  );

  static const title = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 16,
    color: AppColors.white,
  );

  static const subtitle = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 14,
    color: AppColors.white,
  );

  static const textSm = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 14,
    color: AppColors.grey,
  );

  static const text = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 16,
    color: AppColors.grey,
  );
}
