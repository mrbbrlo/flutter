import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:test_project/presentation/home/home_viewmodel.dart';
import 'package:test_project/presentation/home/widgets/partners_list_item.dart';

class HomeBody extends ViewModelWidget<HomeViewModel> {
  const HomeBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, HomeViewModel viewModel) {
    if (viewModel.isBusy) {
      return const Center(child: CircularProgressIndicator());
    }

    return RefreshIndicator(
      onRefresh: () => viewModel.onReady(),
      child: ListView.separated(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          itemBuilder: (context, index) {
            return PartnersListItem(partner: viewModel.partners[index]);
          },
          separatorBuilder: (context, index) {
            return const SizedBox(
              height: 12,
            );
          },
          itemCount: viewModel.partners.length),
    );
  }
}
