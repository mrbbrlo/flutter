import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stacked/stacked.dart';
import 'package:test_project/main/drawables/drawables.dart';
import 'package:test_project/main/styles/index.dart';
import 'package:test_project/presentation/signin/signin_viewmodel.dart';

class PhoneField extends ViewModelWidget<SignInViewModel> {
  const PhoneField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, SignInViewModel viewModel) {
    return TextFormField(
      controller: viewModel.controller,
      keyboardType: TextInputType.number,
      style: TextStyles.subtitle,
      inputFormatters: [viewModel.maskFormatter],
      decoration: InputDecoration(
        isDense: true,
        hintText: "Phone number",
        hintStyle: TextStyles.textSm,
        prefix: const Text(
          '+ ',
          style: TextStyles.subtitle,
        ),
        prefixIcon: Padding(
          padding: const EdgeInsets.all(12),
          child: SvgPicture.asset(
            AppDrawables.phone,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: AppColors.primary),
          borderRadius: BorderRadius.circular(10.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: AppColors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }
}
